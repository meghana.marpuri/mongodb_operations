import pymongo


class DbOperations:
    client = pymongo.MongoClient('localhost', 27017)

    def find(self, db, collection, query):
        dbobj = self.client[db]
        coll = dbobj[collection]
        records = []
        for record in coll.find(query):
            records.append(record)
        print(records)
        return str(records)

    def find_one(self, db, collection, query):

        dbobj = self.client[db]
        coll = dbobj[collection]
        print(coll.find_one(query))
        return coll.find_one(query)

    def remove(self, db, collection, query):
        dbobj = self.client[db]
        coll = dbobj[collection]
        remove = coll.remove(query)
        return remove

    def update(self, db, collection, query):
        dbobj = self.client[db]
        coll = dbobj[collection]
        update = coll.update(query)
        return update

    def insert(self, db, collection, query):
        dbobj = self.client[db]
        coll = dbobj[collection]
        insert = coll.insert(query)
        return insert

