from flask import Blueprint, request

from scripts.handler.db_handler import DbOperations

db_operations = Blueprint('db_blueprint', __name__)

dbobj = DbOperations()


@db_operations.route('/find_docs/', methods=['GET'])
def find_docs():
    db = request.args.get('db')
    collection = request.args.get('collection')
    query = eval(request.args.get('query'))
    response = dbobj.find(db, collection, query)
    return response


@db_operations.route('/findone_docs/', methods=['GET'])
def findone_docs():
    db = request.args.get('db')
    collection = request.args.get('collection')
    query = eval(request.args.get('query'))
    response = dbobj.find_one(db, collection, query)
    return str(response)


@db_operations.route('/update_docs/', methods=['GET'])
def update_docs():
    db = request.args.get('db')
    collection = request.args.get('collection')
    query = eval(request.args.get('query'))
    response = dbobj.update(db, collection, query)
    return str(response)


@db_operations.route('/delete_docs/', methods=['GET'])
def delete_docs():
    db = request.args.get('db')
    collection = request.args.get('collection')
    query = eval(request.args.get('query'))
    response = dbobj.remove(db, collection, query)
    return str(response)


@db_operations.route('/insert_docs/', methods=['GET'])
def insert_docs():
    db = request.args.get('db')
    collection = request.args.get('collection')
    query = eval(request.args.get('query'))
    response = dbobj.insert(db, collection, query)
    return str(response)
