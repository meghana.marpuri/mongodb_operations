from flask import Flask
from scripts.service.db_service import db_operations


app = Flask(__name__)
app.register_blueprint(db_operations)


if __name__ == '__main__':
    app.run()
